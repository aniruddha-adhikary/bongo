from typing import Iterable

from tests.context import Node


def get_path_to_root(node: Node) -> Iterable[Node]:
    # O(log N)
    while node:
        yield node
        node = node.parent


def get_lca(node1: Node, node2: Node) -> Node:
    path1 = list(get_path_to_root(node1))  # O(log N)
    path2 = list(get_path_to_root(node2))  # O(log N)

    if path1[-1] != path2[-1]:  # O(1) Array Access
        raise ValueError('does not share common root')

    for node1 in path1:
        for node2 in path2:
            if node1.value == node2.value:
                return node1
