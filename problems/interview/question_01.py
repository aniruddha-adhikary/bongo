from typing import List, Tuple, Any

import networkx as nx


def get_min_common_point(paths_1, paths_2):
    common_points = set(paths_1).intersection(set(paths_2))
    combined = {k: max(paths_1[k], paths_2[k]) for k in common_points}
    return min(combined, key=lambda x: combined[x])


def get_best_destination(weighted_edges: List[Tuple[Any, Any, int]], a, b) -> Any:
    G = nx.Graph()

    G.add_weighted_edges_from(weighted_edges)

    paths_from_a = nx.single_source_dijkstra_path_length(G, a)
    paths_from_b = nx.single_source_dijkstra_path_length(G, b)

    return get_min_common_point(paths_from_a, paths_from_b)


if __name__ == '__main__':
    EDGES = [
        ['A', 'C', 5],
        ['A', 'E', 3],
        ['A', 'I', 1],
        ['B', 'D', 5],
        ['B', 'E', 9],
        ['B', 'H', 8],
        ['C', 'D', 8],
        ['C', 'E', 6],
        ['G', 'E', 6],
        ['G', 'I', 1],
        ['G', 'H', 3],
    ]

    best = get_best_destination(EDGES, 'A', 'B')

    print(best)
