from typing import List


def find_max(arr: List[int], low=0, high=None) -> int:
    if high is None:
        high = len(arr) - 1

    if high == low:
        return arr[high]

    if abs(high - low) == 1:  # side by side
        return max(arr[low], arr[high])

    mid = (low + high) // 2

    if arr[mid] > arr[mid + 1]:  # pivoted
        return arr[mid]

    if arr[low] > arr[mid]:
        return find_max(arr, low, mid - 1)

    return find_max(arr, mid + 1, high)


if __name__ == '__main__':
    find_max([3, 4, 5, 6, 1, 2], high=5)
