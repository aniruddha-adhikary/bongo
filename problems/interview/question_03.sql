SELECT employees_department.id,
       employees_department.name,
       employees_employee.id,
       employees_employee.name,
       employees_employee.date_of_birth,
       employees_employee.department_id
FROM employees_department
         LEFT JOIN (
    SELECT id,
           name,
           date_of_birth,
           department_id,
           Row_number() OVER (partition by department_id ORDER BY date_of_birth) rn
    FROM employees_employee
) employees_employee ON employees_employee.department_id = employees_department.id
WHERE employees_employee.rn = 1;