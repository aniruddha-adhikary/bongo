from typing import Dict, Any, Union

from problems.question_01 import TraversalResult
from problems import question_01
from tests.context import Person


def traverse(data: Union[Dict[Any, Any], Any], depth: int = 1) -> TraversalResult:
    """
    Depth-first traversal of a Python object or dictionary;
    returns an iterable.
    """

    if not isinstance(data, dict):
        if hasattr(data, '__dict__'):
            data = getattr(data, '__dict__')
        else:
            return []

    for key, value in data.items():
        yield key, depth
        yield from traverse(value, depth + 1)


def print_depth(data: Dict[Any, Any]):
    res = traverse(data)
    print(question_01.to_str(res))


if __name__ == '__main__':
    person_a = Person("User", "1", None)
    person_b = Person("User", "2", person_a)

    a = {
        "key1": 1,
        "key2": {
            "key3": 1,
            "key4": {
                "key5": 4,
                "user": person_b,
            }
        },
    }

    print_depth(a)
