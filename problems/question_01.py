from typing import Dict, Any, Iterable, Tuple

from tests.context import QUESTION_01_EXAMPLE

TraversalResult = Iterable[Tuple[Any, int]]


def traverse(data: Dict[Any, Any], depth: int = 1) -> TraversalResult:
    """
    Depth-first traversal of a Python Dictionary;
    returns an iterable.
    """

    for key, value in data.items():
        yield key, depth

        if isinstance(value, dict):
            yield from traverse(value, depth + 1)


def to_str(res: TraversalResult):
    lines = ['{} {}'.format(k, d) for k, d in res]
    return '\n'.join(lines)


def print_depth(data: Dict[Any, Any]):
    res = traverse(data)
    print(to_str(res))


if __name__ == '__main__':
    print_depth(QUESTION_01_EXAMPLE)
