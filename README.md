# Bongo Coding Test

## Structure

* Solutions are in the `problems` directory. (Yes)
* Tests are in the `tests` directory. (Yes)

## Installation (Prerequisites)

```
$ pip install requirements.txt
```

## Running Tests

```
$ pytest
```