import pytest

from problems.question_01 import traverse, to_str


@pytest.mark.parametrize('data, ret', [
    ({}, []),
    ({'k': 'v'}, [('k', 1)]),
    ({'k': 'v', 'k2': 'v2'}, [('k', 1), ('k2', 1)]),
    ({None: 'v'}, [(None, 1)]),
])
def test_traverse_depth_1(data, ret):
    assert list(traverse(data)) == ret


def test_traverse_depth_2():
    data = {
        'k': 'v',
        'k2': {
            'k21': 'v21'
        }
    }
    ret = [
        ('k', 1),
        ('k2', 1),
        ('k21', 2)
    ]
    assert list(traverse(data)) == ret


def test_traverse_depth_3():
    # Original example in Test Manifesto

    data = {
        "key1": 1,
        "key2": {
            "key3": 1,
            "key4": {
                "key5": 4
            }
        }
    }

    ret = [('key1', 1), ('key2', 1), ('key3', 2), ('key4', 2), ('key5', 3)]

    assert list(traverse(data)) == ret


@pytest.mark.parametrize('res, expected', [
    ([], ''),
    ([('k1', 1)], 'k1 1'),
    ([(None, 1)], 'None 1'),
    ([('k1', 1), ('k0', 99)], 'k1 1\nk0 99'),
])
def test_traverse_str(res, expected):
    assert to_str(res) == expected
