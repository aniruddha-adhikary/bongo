from dataclasses import dataclass
from typing import Any, Optional

QUESTION_01_EXAMPLE = {
    "key1": 1,
    "key2": {
        "key3": 1,
        "key4": {
            "key5": 4
        }
    }
}


class Person(object):
    def __init__(self, first_name, last_name, father):
        self.first_name = first_name
        self.last_name = last_name
        self.father = father


@dataclass
class Node:
    value: Any
    parent: Optional['Node'] = None

    def __repr__(self):
        return '{}'.format(self.value)
