import pytest

from problems.interview.question_02 import find_max


@pytest.mark.parametrize('arr,res', [
    ([1], 1),
    ([1, 2, 3], 3),
    ([1, 2, 3, 0], 3),
    ([3, 4, 5, 6, 1, 2], 6),
])
def test_find_max(arr, res):
    assert find_max(arr) == res
