from problems.interview.question_01 import get_min_common_point


def test_get_min_common_point():
    res = get_min_common_point({'A': 0, 'B': 1}, {'A': 1, 'B': 2})
    assert res == 'A'
