import pytest

from problems.question_03 import get_path_to_root, get_lca
from tests.context import Node


def test_get_path_to_root_empty():
    assert list(get_path_to_root(None)) == []


def test_get_path_to_root_one_node():
    n = Node(5)
    assert list(get_path_to_root(n)) == [n]


def test_get_path_to_root_tree():
    x = Node(3)
    k = Node(2, parent=x)
    c = Node(5, parent=x)
    d = Node(6, parent=c)
    assert list(get_path_to_root(d)) == [d, c, x]
    assert list(get_path_to_root(k)) == [k, x]


def test_get_lca():
    x = Node(3)
    k = Node(2, parent=x)
    c = Node(5, parent=x)
    d = Node(6, parent=c)

    b = Node(99)
    assert get_lca(c, d) == c
    assert get_lca(x, d) == x
    assert get_lca(x, x) == x
    assert get_lca(k, x) == x

    with pytest.raises(ValueError):
        get_lca(d, b)
