from problems.question_02 import traverse
from tests.context import Person


def test_traverse_object():
    person_a = Person("User", "1", None)

    assert list(traverse(person_a)) == [
        ('first_name', 1),
        ('last_name', 1),
        ('father', 1),
    ]


def test_traverse_nested_obj():
    person_a = Person("User", "1", None)
    person_b = Person("User", "2", person_a)

    assert list(traverse(person_b, depth=4)) == [
        ('first_name', 4),
        ('last_name', 4),
        ('father', 4),
        ('first_name', 5),
        ('last_name', 5),
        ('father', 5),
    ]


def test_traverse_dict_with_nested_obj():
    person_a = Person("User", "1", None)
    person_b = Person("User", "2", person_a)

    a = {
        "key1": 1,
        "key2": {
            "key3": 1,
            "key4": {
                "key5": 4,
                "user": person_b,
            }
        },
    }

    assert list(traverse(a)) == [
        ('key1', 1),
        ('key2', 1),
        ('key3', 2),
        ('key4', 2),
        ('key5', 3),
        ('user', 3),
        ('first_name', 4),
        ('last_name', 4),
        ('father', 4),
        ('first_name', 5),
        ('last_name', 5),
        ('father', 5),
    ]
